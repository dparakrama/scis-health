package basicTests;

import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

/**
 * Author: dparakrama Created : May 21, 2018
 */
public class TestListener implements ITestListener {

	WebDriver driver = null;
	String filePath = "\\src\\test\\resources\\";
	String curDir = System.getProperty("user.dir");

	@Override
	public void onTestFailure(ITestResult result) {
		System.out.println("***** ERROR " + result.getName()
				+ " TEST HAS FAILED*****");
		try {
			String methodName = result.getName().toString().trim();
			takeScreenShot(methodName);
			Mailer.sendAMail(methodName);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void takeScreenShot(String methodName) throws IOException {
		driver = BasicTestObjects.getDriver();
		File scrFile = ((TakesScreenshot) driver)
				.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(scrFile, new File(curDir + filePath + methodName
				+ ".png"));

	}

	public void onFinish(ITestContext context) {
	}

	public void onTestStart(ITestResult result) {
	}

	public void onTestSuccess(ITestResult result) {
	}

	public void onTestSkipped(ITestResult result) {
	}

	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
	}

	public void onStart(ITestContext context) {
	}

}
