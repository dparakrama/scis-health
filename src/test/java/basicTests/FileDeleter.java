package basicTests;

import java.io.File;

/**
 * Author: dparakrama Created : Apr 5, 2018
 */
public class FileDeleter {

	public static void fileDeleter(String filename) {
		{
			try {
				File file = new File(filename);
				if (file.delete()) {
					System.out.println(file.getName() + "IS DELETED!");
				} else {
					System.out.println("DELETE OPERATION FAILED!");
				}
			} catch (Exception e) {
				e.printStackTrace();

			}

		}

	}
}