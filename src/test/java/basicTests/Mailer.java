package basicTests;

import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 * Author: dparakrama Created : May 21, 2018
 */
public class Mailer {
	static String filePath = "\\src\\test\\resources\\";
	static String curDir = System.getProperty("user.dir");

	public static void sendAMail(String methodName) {

		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.socketFactory.class",
				"javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", "465");

		Session session = Session.getDefaultInstance(props,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(
								"esatestteam2@gmail.com", "1qaz2wsx@");

					}

				});

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress("esatestteam@gmail.com"));
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse("daupadi.parakrama@esa.edu.au, jesse.licot@esa.edu.au, Puneet.Jain@esa.edu.au, Frankie.Yip@esa.edu.au"));
			message.setSubject("SCIS HEALTH CHECK FALIURE | SCREENSHOT");
			BodyPart messageBodyPart1 = new MimeBodyPart();
			messageBodyPart1.setText("PLESE FIND THE ATTACHMENTS");
			Multipart multipart = new MimeMultipart();
			addAttachment(multipart, curDir + filePath + methodName + ".png");
			message.setContent(multipart);
			Transport.send(message);
			System.out.println("=====Email Sent=====");
			FileDeleter.fileDeleter(curDir + filePath + methodName + ".png");

		} catch (MessagingException e) {
			throw new RuntimeException(e);

		}
	}

	private static void addAttachment(Multipart multipart, String filename)
			throws MessagingException {
		DataSource source = new FileDataSource(filename);
		MimeBodyPart messageBodyPart = new MimeBodyPart();
		messageBodyPart.setDataHandler(new DataHandler(source));
		messageBodyPart.setFileName(filename);
		multipart.addBodyPart(messageBodyPart);

	}
}
