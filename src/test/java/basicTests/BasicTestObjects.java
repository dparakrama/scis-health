package basicTests;

import io.github.bonigarcia.wdm.WebDriverManager;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import com.relevantcodes.extentreports.LogStatus;

public class BasicTestObjects{
	protected static WebDriver driver;
	
	
	public static WebDriver getDriver() {
		return driver;
	}
	public static void browserSetup(String Url) throws InterruptedException {
		WebDriverManager.chromedriver().setup();
		ChromeOptions options = new ChromeOptions();
		options.addArguments("--window-size=1920,1080");
		options.addArguments("--disable-features=VizDisplayCompositor");
		options.addArguments("--start-maximized");
		options.addArguments("--headless");
        driver = new ChromeDriver(options);
		driver.manage().window().maximize();
		driver.get(Url);
		Thread.sleep(5000);
	}

	public void closeSession() {
		driver.quit();
	}

	
	public void takeAScreenShot (String FileName) throws IOException {
		    File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(scrFile, new File("C:\\ScreenShots\\"+ FileName));
            
 		
	}
	
	 public static boolean IsElementPresent(String Xpath){
	 if(driver.findElements(By.xpath(Xpath)).size()!= 0){
		 System.out.println("ELEMENT IS PRESENT");
		 return true;
		 }else{
		 System.out.println("ELEMENT IS ABSENT");
		 return false;
		 }
	
	
	
}
}
	
	


