package basicTests;

import java.io.File;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

public class ExtentManager {

	protected static ExtentReports report;
	public static String _reportFileName;
	protected ExtentTest test;

	public static ExtentReports getInstance() {
		String curDir = System.getProperty("user.dir");
		String _reportsPathFile = "\\scis-regression\\reports\\test-automation.html";
		String repPath = curDir + _reportsPathFile;
		File repFilePath = new File(repPath);
		if (repFilePath.exists()) {
			repFilePath.delete();
		}
		String reportFileName = repPath;
		_reportFileName = reportFileName;
		if (report == null) {
			report = new ExtentReports(reportFileName, true);

		}

		return report;
	}

}
