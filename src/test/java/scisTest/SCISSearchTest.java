package scisTest;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import scisPageObjects.SearchPage;
import basicTests.BasicTestObjects;

import com.relevantcodes.extentreports.LogStatus;

public class SCISSearchTest extends BasicTestObjects {
	static String Xpath = "html/body/div[1]/div/div/div[3]/button";

	@Parameters({ "MySCISURL", "MySCISUserName", "MySCISPassword" })
	@Test
	public void verifySearchPage(String URL, String UserName, String Password)
			throws Exception {
		browserSetup(URL);
		SearchPage searchPage = new SearchPage(driver);
		searchPage.doLogin(UserName, Password);
		//test.log(LogStatus.PASS, "LOGGED IN AS A USER");
		Thread.sleep(18000);
		if (IsElementPresent()) {
			Actions actions = new Actions(driver);
			actions.moveToElement(driver.findElement(By.xpath(Xpath)));
			actions.click();
			actions.build().perform();
			Thread.sleep(4000);
		}
		verifySearchWorkFlow(searchPage);
		closeSession();
	}

	private void verifySearchWorkFlow(SearchPage searchPage)
			throws InterruptedException, IOException, Exception {
		searchPage.setSearchCriteria("xxc");
		//test.log(LogStatus.PASS, "SEARCHED FOR XXC");
		Thread.sleep(10000);

		if (driver.findElement(By.xpath("//*[contains(text(), 'Canada')]"))
				.isDisplayed()) {
			Thread.sleep(5000);
			takeAScreenShot("Searchresults.png");
		//	test.log(LogStatus.PASS, "VERIFIED RESULTS");
		} else {
		//	test.log(LogStatus.FAIL, "CANADA DID NOT LOAD");
			throw new Exception();
		}

		Thread.sleep(10000);
		searchPage.verifyanAdvancedSearch();
	}
	
	 public static boolean IsElementPresent(){
		 if(driver.findElements(By.xpath(Xpath)).size()!= 0){
			 System.out.println("ELEMENT IS PRESENT");
			 return true;
			 }else{
			 System.out.println("ELEMENT IS ABSENT");
			 return false;
			 }
		
		
}
}
