package scisTest;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;








import scisPageObjects.BookDownloadPage;

import com.relevantcodes.extentreports.LogStatus;

import basicTests.BasicTestObjects;

public class SICSDownloadPageTest extends BasicTestObjects {
	static String Xpath = "/html/body/div[1]/div/div/div[1]/button/span";
	
	@Parameters({ "MySCISURL", "MySCISUserName", "MySCISPassword", "ISBN" })
	@Test
	public void verifyDownloadPage(String URL, String UserName,
			String Password, String ISBN) throws InterruptedException, IOException {
		browserSetup(URL);
		BookDownloadPage bookDownloadPage = new BookDownloadPage(driver);
		bookDownloadPage.doLogin(UserName, Password);
		//test.log(LogStatus.PASS, "LOGGED IN AS A USER");
		Thread.sleep(18000);
		if (IsElementPresent()) {
			Actions actions = new Actions(driver);
			actions.moveToElement(driver.findElement(By.xpath(Xpath)));
			actions.click();
			actions.build().perform();
			Thread.sleep(4000);
		}
		
		bookDownloadPage.setDownloadCriteria(ISBN);
	//	test.log(LogStatus.PASS, "DOWNLOADED THE BOOK ISBN : " + ISBN);
		closeSession();
		
	}
	

	 public static boolean IsElementPresent(){
	 if(driver.findElements(By.xpath(Xpath)).size()!= 0){
		 System.out.println("ELEMENT IS PRESENT");
		 return true;
		 }else{
		 System.out.println("ELEMENT IS ABSENT");
		 return false;
		 }
	
	
}
}

