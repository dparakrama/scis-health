package scisTest;

import java.io.IOException;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import scisPageObjects.BarcodeScannerPage;
import scisPageObjects.SearchPage;
import com.relevantcodes.extentreports.LogStatus;
import basicTests.BasicTestObjects;

public class SCISBarcodeScannersTest extends BasicTestObjects {

	static String Xpath = "/html/body/div[1]/div/div/div[1]/button/span";

	@Parameters({ "MySCISURL", "MySCISUserName", "MySCISPassword" })
	@Test
	public void verifyProducts(String URL, String UserName, String Password)
			throws Exception {
		browserSetup(URL);
		SearchPage searchPage = new SearchPage(driver);
		searchPage.doLogin(UserName, Password);
		//test.log(LogStatus.PASS, "LOGGED IN AS A USER");
		Thread.sleep(18000);
		if (IsElementPresent()) {
			Actions actions = new Actions(driver);
			actions.moveToElement(driver.findElement(By.xpath(Xpath)));
			actions.click();
			actions.build().perform();
			Thread.sleep(4000);
		}

		verifybarcodeScannerFlow();
		driver.get(URL);
		WebElement ele = driver.findElement(By.xpath("//*[@class='basketitems']"));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", ele);
		Thread.sleep(3000);
		WebElement ele2 = driver.findElement(By.xpath("//*[@id='cartItems']/div/div/div[1]/div/div[1]/h3/a"));
	    executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", ele2);
		Thread.sleep(3000);
		closeSession();
	}

	private void verifybarcodeScannerFlow() throws InterruptedException,
			IOException {
		BarcodeScannerPage BarcodeScannerPage = new BarcodeScannerPage(driver);
		//BarcodeScannerPage.clickBarcodeScanners();
		driver.get("https://www.scisdata.com/barcode-scanners/");
		Thread.sleep(5000);
		BarcodeScannerPage.verifyAllPageElements();
		//test.log(LogStatus.PASS, "VERIFIED ALL ACTIVE ELEMENTS");
		takeAScreenShot("BarcodeScanners.png");
		BarcodeScannerPage.verifyABarcodeScanner();
		WebElement ele = driver.findElement(By.xpath("//*[@id='btnProceed']"));
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", ele);
		Thread.sleep(3000);
		BarcodeScannerPage.verifyCheckout();
		Thread.sleep(3000);
		//test.log(LogStatus.PASS, "VERIFIED A CHECKOUT");
		
		
		
		
	}
	
	
	
	 public static boolean IsElementPresent(){
	 if(driver.findElements(By.xpath(Xpath)).size()!= 0){
		 System.out.println("ELEMENT IS PRESENT");
		 return true;
		 }else{
		 System.out.println("ELEMENT IS ABSENT");
		 return false;
		 }
	
	
	
}
}