package scisPageObjects;

import org.openqa.selenium.WebDriver;
import basicPageObjects.BasicPageObjects;



public class SearchPage extends BasicPageObjects {
	
	/**
	 * Page Elements
	 */	
	String btnLogin = "//*[@id='header']/div/div/nav/div[2]/div[1]/ul/li/a";
	String txtUserName = "//*[@id='username']";
	String txtPassowrd = "//*[@id='password']";
	String btnSignIn = "//*[@id='fm1']/input[3]";
	String btnSearch = "html/body/div[1]/div/div/div/div/ul/li[1]/a";
	String txtSearch = ".//*[@id='queryForDisplay']";
	String btnSearchforCriteria = ".//*[@id='hero-search']/div/div/button";
	String txtCountry = ".//*[@id='accordiongroup-694-114-panel']/div/ul/li/span/span/a";	
	String btnAdvanced = ".//*[@id='content']/search-bar/span/discover-nav/div/div/ul/li[2]/a";
	String txtSearchKeywords = ".//*[@id='keywords0']";
	String txtKeyword1 = ".//*[@id='keywords1']";
	String drpKeyword1= ".//*[@id='field0']";	
	String drpValue1= "//*[text()[contains(.,'Title')]]";		
	String drpKeyword2 = ".//*[@id='field1']";
	String drpValue2= "//*[text()[contains(.,'SCIS Number')]]";	
	String drpKeyword3 = ".//*[@id='field2']";
	String drpValue3= "//*[text()[contains(.,'Author')]]";
	String drpType1 = ".//*[@id='type0']";
	String drpType2 = ".//*[@id='type1']";
	String drpType3 = ".//*[@id='type2']";
	String drpPlaceOfBup = ".//*[@id='placeOfPublication']/div/button";
	String drpPlaceOfBupValue = ".//*[@id='placeOfPublication']/div/button";
	String drpFrom = "//*[text()[contains(.,'Canada')]]";	
	String btnAdvancedSearch = ".//*[@id='advanced-search']/form/div[2]/div/button[1]";
	String txtResult = ".//*[@id='content']/div/div/div[2]/div[3]/ul/li/div/div[1]/div[2]/div[1]/a";
	
	public SearchPage(WebDriver driver) {
		super(driver);
	}

	public void doLogin(String UserName, String Password)
			throws InterruptedException {
		clickButton(btnLogin);
		enterText(txtUserName, UserName);
		enterText(txtPassowrd, Password);
		clickButton(btnSignIn);
	}

	public void setSearchCriteria(String text) throws InterruptedException {
		clickButton(btnSearch);
		enterText(txtSearch, text);
		clickButton(btnSearchforCriteria);
	}

	public void verifySearchResults() throws InterruptedException {
		isElementPresent(txtCountry, "Canada");

	}
	
	public void verifyanAdvancedSearch() throws InterruptedException {
		clickButton(btnSearch);
		Thread.sleep(4000);
		clickButton(btnAdvanced);
		Thread.sleep(4000);
		enterText(txtSearchKeywords, "Dogs");
		Thread.sleep(4000);
		clickButton(drpKeyword1);
		Thread.sleep(4000);
		clickButton(drpValue1);
		Thread.sleep(4000);
		enterText(txtKeyword1, "1147451");
		Thread.sleep(4000);
		clickButton(btnAdvancedSearch);
		Thread.sleep(4000);
		getContent(txtResult,"Song dogs");
		Thread.sleep(4000);
		
	}
}
