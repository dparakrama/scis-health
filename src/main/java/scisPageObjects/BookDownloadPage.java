package scisPageObjects;

import org.openqa.selenium.WebDriver;
import basicPageObjects.BasicPageObjects;

public class BookDownloadPage extends BasicPageObjects {
	
	/**
	 *Page Elements
	 */
	String btnLogin = "//*[@id='header']/div/div/nav/div[2]/div[1]/ul/li/a";
	String txtUserName = "//*[@id='username']";
	String txtPassowrd = "//*[@id='password']";
	String btnSignIn = "//*[@id='fm1']/input[3]";
	String btnCreateRequest = "//*[@id='content']/div/div/div[2]/form/button";
	String txtDownloadName = "//*[@id='scan']"; 
	String lblDownloadRecord = "//*[@id='content']/div[2]/div/div/div/div/div[1]/div[1]/div"; 
	String btnDownload =".//*[@class='btn btn-default btn-lg bulk-action']";

	public BookDownloadPage(WebDriver driver) {
		super(driver);
	}

	public void doLogin(String UserName, String Password)
			throws InterruptedException {
		clickButton(btnLogin);
		enterText(txtUserName, UserName);
		enterText(txtPassowrd, Password);
		clickButton(btnSignIn);
	}

	public void setDownloadCriteria(String ISBN) throws InterruptedException {
		enterText(txtDownloadName, ISBN);
		getContent(btnCreateRequest, "Create request");
		/* clickButton(btnCreateRequest);
		 * Thread.sleep(120000);
		 * getContent(lblDownloadRecord,"1 records have been matched successfully");
		 * clickButton(btnDownload);
		 * Thread.sleep(5000);*/
	}

	
	
	

}
