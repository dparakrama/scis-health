package scisPageObjects;

import org.openqa.selenium.WebDriver;
import basicPageObjects.BasicPageObjects;



public class BarcodeScannerPage extends BasicPageObjects {
	/**
	 *Page Elements
	 */
	//String btnBarcodeScanners = "//*[@id='navigation']/ul/li[3]/a";
	String txtBatcodeScanners = ".//*[@id='content']/div[1]/div/div/div/h1";
	String btnBarcodeScanner1 = "//*[@id='content']/div[2]/div[2]/div/div/div[1]/div/a";
	String btnBuyNow = ".//*[@id='btnBuyNow']";
	String btnShoppingCartItem = "//*[contains(text(), 'Cipherlab 1560P portable wireless with adjustable stand')]";
    String btnProceedToCheckout = ".//*[@class='btn btn-lg btn-block btn-success']";
	String btnProceed = ".//*[@id='btnProceed']";
	String txtheader = "//*[contains(text(), 'Checkout')]";
	String btnProceedCo = "//*[@class='btn btn-primary btn-lg']"; 
	String btnAgreeTermsOfUse = "//*[@id='btnAgreeTermsOfUse']"; 
	String btnSecurityForm= "//*[@id='securePayForm']/div/div/div/input[2]"; 
    String txtInvoicePayment= "//*[@id='header']/h1"; 

	public BarcodeScannerPage(WebDriver driver) {
		super(driver);
	}

	//public void clickBarcodeScanners() throws InterruptedException {
	//	clickButton(btnBarcodeScanners);

	//}

	public void verifyAllPageElements () throws InterruptedException {
		isElementPresent(txtBatcodeScanners, "Barcode scanners");

	}
	
	public void verifyABarcodeScanner() throws InterruptedException {
		clickButton(btnBarcodeScanner1); 
		clickButton(btnBuyNow);
		isAvailable(btnShoppingCartItem);
		clickButton(btnProceedToCheckout);
		isAvailable(txtheader);
		
	}
	
	public void verifyCheckout() throws InterruptedException {
	/*	clickButton(btnAgreeTermsOfUse);
		clickButton(btnSecurityForm);
		getContent(txtInvoicePayment,"Invoice Payment");*/
	}
	

}
