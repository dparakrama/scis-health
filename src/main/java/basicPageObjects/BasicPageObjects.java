package basicPageObjects;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class BasicPageObjects {
	private WebDriver driver;

	public BasicPageObjects(WebDriver driver) {
		this.driver = driver;
	}

	public void enterText(String TextboxXpath, String Text) {
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath(TextboxXpath)))
				.clear();
		wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath(TextboxXpath)))
				.sendKeys(Text);
	}

	public void clickButton(String ButtonXpath) {
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath(ButtonXpath)))
				.click();
	}

	public void clickButtonbyCss(String ButtonCSS) {
		WebDriverWait wait = new WebDriverWait(driver, 60);
		wait.until(
				ExpectedConditions.elementToBeClickable(By
						.cssSelector(ButtonCSS))).click();
	}

	public void getContent(String Labelxpath, String Text)
			throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 60);
		String content = wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath(Labelxpath)))
				.getText();
		Assert.assertTrue(content.contains(Text));
		}

	public void getContentbyAttribute(String Labelxpath, String Text)
			throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 60);
		String content = wait
				.until(ExpectedConditions.elementToBeClickable(By
						.xpath(Labelxpath))).getAttribute("value").toString();
		Assert.assertTrue(content.contains(Text));


	}

	public void findTableElement(String tableXpath, String searchStringValue,
			String firstXpathBeforRowNumber, String LastXpathBeforeRowNumber)
			throws InterruptedException {
		boolean breakIt = true;
		while (true) {
			breakIt = true;
			try {
				WebDriverWait wait = new WebDriverWait(driver, 60);
				List<WebElement> rows_table = wait.until(
						ExpectedConditions.elementToBeClickable(By
								.xpath(tableXpath))).findElements(
						By.tagName("tr"));
				int rows_count = rows_table.size();

				for (int row = 0; row < rows_count; row++) {
					List<WebElement> Columns_row = rows_table.get(row)
							.findElements(By.tagName("td"));
					int columns_count = Columns_row.size();

					for (int column = 0; column < columns_count; column++) {
						String celltext = Columns_row.get(column).getText();
						System.out.println(celltext);
						if (celltext.contains(searchStringValue)) {
							driver.findElement(
									By.xpath(firstXpathBeforRowNumber + row
											+ LastXpathBeforeRowNumber))
									.click();
							Thread.sleep(4000);
						}

					}
				}
			} catch (Exception e) {
				if (e.getMessage().contains("ELEMENT IS NOT ATTACHED")) {
					breakIt = false;
				}
			}
			if (breakIt) {
				break;
			}
		}

	}

	public boolean isAvailable(String xpath) {
		WebDriverWait wait = new WebDriverWait(driver, 60);
		if (wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpath)))
				.isDisplayed()) {
			return true;
		} else {
			return false;
		}

	}

	public boolean isElementPresent(String xpath, String text)
			throws InterruptedException {
		WebDriverWait wait = new WebDriverWait(driver, 60);
		String content = wait.until(
				ExpectedConditions.elementToBeClickable(By.xpath(xpath)))
				.getText();
		if (content.contains(text)) {
			return true;
		} else {
			throw new InterruptedException();
		}

	}

}
